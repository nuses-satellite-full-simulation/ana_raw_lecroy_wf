RELVERSION  = $(shell cat .release)

ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTLIBS      = $(shell $(ROOTSYS)/bin/root-config --libs)
ROOTGLIBS     = $(shell $(ROOTSYS)/bin/root-config --glibs)

MakefileFullPath = $(abspath $(lastword $(MAKEFILE_LIST)))
MakefileDirFullPath = $(shell dirname $(MakefileFullPath))
INSTALLDIR = $(MakefileDirFullPath)/install.$(RELVERSION)/

CXX  = g++
CXX += -I./

CXXFLAGS  = -g -Wall -fPIC -Wno-deprecated
CXXFLAGS += $(ROOTCFLAGS)
CXXFLAGS += $(ROOTLIBS)
CXXFLAGS += $(ROOTGLIBS)
#CXXFLAGS += -std=c++14
CXXFLAGS += -fconcepts

OUTLIB = ./obj/

#----------------------------------------------------#

all: makedir convert_raw_txt_wf_root convert_csv_wf_root

makedir:
	mkdir -p $(OUTLIB);

.PHONY: printmakehelp_and_reminder
printmakehelp_and_reminder:  convert_raw_txt_wf_root.cc Makefile
	$(info  /******************************************************************************/)
	$(info  * task --> printmakehelp_and_reminder: convert_raw_txt_wf_root.cc Makefile    *)
	$(info  * $$@ ----> $@                                         *)
	$(info  * $$< --------------------------------> $<             *)
	$(info  * $$^ --------------------------------> $^    *)
	$(info  /******************************************************************************/)

obj/convert_raw_txt_wf_root.o: convert_raw_txt_wf_root.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) 

obj/libconvert_raw.so: obj/convert_raw_txt_wf_root.o 
	$(CXX) -shared -o $@ $^

convert_raw_txt_wf_root: convert_raw_txt_wf_root.cc
	$(CXX) -o $@ $^ $(CXXFLAGS)

convert_csv_wf_root: convert_csv_wf_root.cc
	$(CXX) -o $@ $^ $(CXXFLAGS)

install: makedir obj/libconvert_raw.so
	mkdir -p $(INSTALLDIR);
	cp $(OUTLIB)libconvert_raw.so $(INSTALLDIR)libconvert_raw.so
	cp src/*.hh $(INSTALLDIR).

cleaninstall:
	rm -rf $(INSTALLDIR)

clean:
	rm -f convert_raw_txt_wf_root
	rm -f *~
	rm -f .*~
	rm -f $(OUTLIB)*.o
	rm -f $(OUTLIB)*.so
