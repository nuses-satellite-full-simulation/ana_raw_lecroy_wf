#!/bin/bash

function printHelp {
    echo " --> ERROR in input arguments "
    echo " [0] --20.04.2023_01 : convert data"
    echo " [0] --20.04.2023_02 : convert data"
    echo " [0] --20.04.2023_03 : convert data"
    echo " [0] --20.04.2023_04 : convert data"
    echo " [0] --20.04.2023_05 : convert data"
    echo " [0] -h              : print help"
}

if [ $# -eq 0 ] 
then    
    printHelp
else
    if [ "$1" = "--20.04.2023_01" ]; then
	#rm -rf convert_raw_txt_wf_root.log
	#data_dir="../data/20.04.2023_01/"
	#for file_to_convert in `ls $data_dir*.txt`; do
	#echo "$file_to_convert"
	#time ./convert_raw_txt_wf_root 0 $file_to_convert | tee -a convert_raw_txt_wf_root.log
	#done
	dirName="20.04.2023_01"
	out_log="../data/$dirName/convert_raw_txt_wf_root.log"
	rm -rf $out_log
	data_dir="../data/$dirName/"
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00000.txt -2.0034e-10 -1.9507e-10 -0.01490 -0.0135 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00001.txt -0.1680e-9  -0.1560e-9  -0.01455 -0.0130 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00002.txt -0.1680e-8  -0.1250e-9  -0.01500 -0.0130 | tee -a $out_log
    elif [ "$1" = "--20.04.2023_02" ]; then
	dirName="20.04.2023_02"
	out_log="../data/$dirName/convert_raw_txt_wf_root.log"
	rm -rf $out_log
	data_dir="../data/$dirName/"
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00000.txt  -0.1e-9   -0.094e-9 -0.01400 -0.01225 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00001.txt -88.0e-12  -82.0e-12 -0.01425 -0.01250 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00002.txt -67.0e-12  -58.0e-12 -0.01350 -0.01140 | tee -a $out_log
    elif [ "$1" = "--20.04.2023_03" ]; then
	dirName="20.04.2023_03"
	out_log="../data/$dirName/convert_raw_txt_wf_root.log"
	rm -rf $out_log
	data_dir="../data/$dirName/"
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00000.txt  -0.10e-9  -0.093e-9  -0.01550 -0.01380 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00001.txt -70.0e-12 -62.000e-12 -0.01400 -0.01220 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00002.txt -54.0e-12 -45.000e-12 -0.01450 -0.01275 | tee -a $out_log
    elif [ "$1" = "--20.04.2023_04" ]; then
	dirName="20.04.2023_04"
	out_log="../data/$dirName/convert_raw_txt_wf_root.log"
	rm -rf $out_log
	data_dir="../data/$dirName/"
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00000.txt -8.3871370e-11 -8.0221830e-11 -0.01600 -0.01400 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00001.txt -4.1646450e-11 -3.7511150e-11 -0.01400 -0.01225 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00002.txt -2.0502200e-11 -1.6445400e-11 -0.01450 -0.01250 | tee -a $out_log
    elif [ "$1" = "--20.04.2023_05" ]; then
	dirName="20.04.2023_05"
	out_log="../data/$dirName/convert_raw_txt_wf_root.log"
	rm -rf $out_log
	data_dir="../data/$dirName/"
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00000.txt -1.8655220e-11 -1.4953380e-11 -0.0160 -0.0140 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00001.txt  4.1136630e-11  4.5105970e-11 -0.0140 -0.0120 | tee -a $out_log
	./convert_raw_txt_wf_root 0 $data_dir/C1wf00002.txt  8.3894720e-11  8.9046680e-11 -0.0140 -0.0125 | tee -a $out_log
    elif [ "$1" = "-h" ]; then
        printHelp
    else
        printHelp
    fi
fi

#espeak "I have done"
