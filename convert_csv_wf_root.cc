//
#include <TH1D.h>
#include <TStyle.h>
#include <TString.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>

//C, C++
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <vector>

using namespace std;

void conv_csv_to_gr(TString txtFile, TGraph *gr, Bool_t if_conv_to_ns = false, Double_t t_shift_ns = 0.0, Bool_t if_norm = false);

int main(int argc, char *argv[]){
  if(argc == 2 && atoi(argv[1]) == 0){
    //UniGe_VS_Model.csv
    TGraph *gr_FBK_FBKampl_25um_Lpar5em09R5 = new TGraph();
    gr_FBK_FBKampl_25um_Lpar5em09R5->SetNameTitle("gr_FBK_FBKampl_25um_Lpar5em09R5","gr_FBK_FBKampl_25um_Lpar5em09R5");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_25um_Lpar5e-09R5.csv",gr_FBK_FBKampl_25um_Lpar5em09R5);
    //
    TGraph *gr_FBK_FBKampl_25um_Lpar5em08R5 = new TGraph();
    gr_FBK_FBKampl_25um_Lpar5em08R5->SetNameTitle("gr_FBK_FBKampl_25um_Lpar5em08R5","gr_FBK_FBKampl_25um_Lpar5em08R5");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_25um_Lpar5e-08R5.csv",gr_FBK_FBKampl_25um_Lpar5em08R5);
    //
    TGraph *gr_FBK_FBKampl_25um_Lpar5em08R10 = new TGraph();
    gr_FBK_FBKampl_25um_Lpar5em08R10->SetNameTitle("gr_FBK_FBKampl_25um_Lpar5em08R10","gr_FBK_FBKampl_25um_Lpar5em08R10");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_25um_Lpar5e-08R10.csv",gr_FBK_FBKampl_25um_Lpar5em08R10);
    //
    TGraph *gr_FBK_FBKampl_25um_Lpar5em08R15 = new TGraph();
    gr_FBK_FBKampl_25um_Lpar5em08R15->SetNameTitle("gr_FBK_FBKampl_25um_Lpar5em08R15","gr_FBK_FBKampl_25um_Lpar5em08R15");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_25um_Lpar5e-08R15.csv",gr_FBK_FBKampl_25um_Lpar5em08R15);
    //
    TGraph *gr_FBK_FBKampl_25um_Lpar2p5em08R50 = new TGraph();
    gr_FBK_FBKampl_25um_Lpar2p5em08R50->SetNameTitle("gr_FBK_FBKampl_25um_Lpar2p5em08R50","gr_FBK_FBKampl_25um_Lpar2p5em08R50");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_25um_Lpar2.5e-08R50.csv",gr_FBK_FBKampl_25um_Lpar2p5em08R50);
    //
    TGraph *gr_FBK_FBKampl_25um_measurements = new TGraph();
    gr_FBK_FBKampl_25um_measurements->SetNameTitle("gr_FBK_FBKampl_25um_measurements","gr_FBK_FBKampl_25um_measurements");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_25um_measurements.csv",gr_FBK_FBKampl_25um_measurements);
    //
    //    
    //40um 3x3
    //../data/20.04.2023_01/C1wf00000.txt_norm.dat 40 V (32.6 Vbd)
    //../data/20.04.2023_01/C1wf00001.txt_norm.dat 44 V (32.6 Vbd)
    //../data/20.04.2023_01/C1wf00002.txt_norm.dat 48 V (32.6 Vbd)
    //
    TGraph *gr_FBKampl_40um_meas40V = new TGraph();
    TGraph *gr_FBKampl_40um_meas44V = new TGraph();
    TGraph *gr_FBKampl_40um_meas48V = new TGraph();
    gr_FBKampl_40um_meas40V->SetNameTitle("gr_FBKampl_40um_meas40V","gr_FBKampl_40um_meas40V");
    gr_FBKampl_40um_meas44V->SetNameTitle("gr_FBKampl_40um_meas44V","gr_FBKampl_40um_meas44V");
    gr_FBKampl_40um_meas48V->SetNameTitle("gr_FBKampl_40um_meas48V","gr_FBKampl_40um_meas48V");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_40um_meas40V_C1wf00000.txt_norm.dat", gr_FBKampl_40um_meas40V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_40um_meas44V_C1wf00001.txt_norm.dat", gr_FBKampl_40um_meas44V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_40um_meas48V_C1wf00002.txt_norm.dat", gr_FBKampl_40um_meas48V, true, -300.0);
    //
    //25um 1x1
    //../data/20.04.2023_02/C1wf00000.txt_norm.dat 40 V (32.6 Vbd)
    //../data/20.04.2023_02/C1wf00001.txt_norm.dat 44 V (32.6 Vbd)
    //../data/20.04.2023_02/C1wf00002.txt_norm.dat 48 V (32.6 Vbd)
    TGraph *gr_FBKampl_25um_meas40V = new TGraph();
    TGraph *gr_FBKampl_25um_meas44V = new TGraph();
    TGraph *gr_FBKampl_25um_meas48V = new TGraph();
    gr_FBKampl_25um_meas40V->SetNameTitle("gr_FBKampl_25um_meas40V","gr_FBKampl_25um_meas40V");
    gr_FBKampl_25um_meas44V->SetNameTitle("gr_FBKampl_25um_meas44V","gr_FBKampl_25um_meas44V");
    gr_FBKampl_25um_meas48V->SetNameTitle("gr_FBKampl_25um_meas48V","gr_FBKampl_25um_meas48V");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_25um_meas40V_C1wf00000.txt_norm.dat", gr_FBKampl_25um_meas40V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_25um_meas44V_C1wf00001.txt_norm.dat", gr_FBKampl_25um_meas44V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_25um_meas48V_C1wf00002.txt_norm.dat", gr_FBKampl_25um_meas48V, true, -300.0);    
    //30um 1x1
    //../data/20.04.2023_03/C1wf00000.txt_norm.dat 40 V (32.6 Vbd)
    //../data/20.04.2023_03/C1wf00001.txt_norm.dat 44 V (32.6 Vbd)
    //../data/20.04.2023_03/C1wf00002.txt_norm.dat 48 V (32.6 Vbd)
    TGraph *gr_FBKampl_30um_meas40V = new TGraph();
    TGraph *gr_FBKampl_30um_meas44V = new TGraph();
    TGraph *gr_FBKampl_30um_meas48V = new TGraph();
    gr_FBKampl_30um_meas40V->SetNameTitle("gr_FBKampl_30um_meas40V","gr_FBKampl_30um_meas40V");
    gr_FBKampl_30um_meas44V->SetNameTitle("gr_FBKampl_30um_meas44V","gr_FBKampl_30um_meas44V");
    gr_FBKampl_30um_meas48V->SetNameTitle("gr_FBKampl_30um_meas48V","gr_FBKampl_30um_meas48V");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_30um_meas40V_C1wf00000.txt_norm.dat", gr_FBKampl_30um_meas40V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_30um_meas44V_C1wf00001.txt_norm.dat", gr_FBKampl_30um_meas44V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_30um_meas48V_C1wf00002.txt_norm.dat", gr_FBKampl_30um_meas48V, true, -300.0);
    //35mu 1x1
    //../data/20.04.2023_04/C1wf00000.txt_norm.dat 40 V (32.6 Vbd)
    //../data/20.04.2023_04/C1wf00001.txt_norm.dat 44 V (32.6 Vbd)
    //../data/20.04.2023_04/C1wf00002.txt_norm.dat 48 V (32.6 Vbd)
    TGraph *gr_FBKampl_35um_meas40V = new TGraph();
    TGraph *gr_FBKampl_35um_meas44V = new TGraph();
    TGraph *gr_FBKampl_35um_meas48V = new TGraph();
    gr_FBKampl_35um_meas40V->SetNameTitle("gr_FBKampl_35um_meas40V","gr_FBKampl_35um_meas40V");
    gr_FBKampl_35um_meas44V->SetNameTitle("gr_FBKampl_35um_meas44V","gr_FBKampl_35um_meas44V");
    gr_FBKampl_35um_meas48V->SetNameTitle("gr_FBKampl_35um_meas48V","gr_FBKampl_35um_meas48V");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_35um_meas40V_C1wf00000.txt_norm.dat", gr_FBKampl_35um_meas40V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_35um_meas44V_C1wf00001.txt_norm.dat", gr_FBKampl_35um_meas44V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_35um_meas45V_C1wf00002.txt_norm.dat", gr_FBKampl_35um_meas48V, true, -300.0);
    //50um 1x1
    //../data/20.04.2023_05/C1wf00000.txt_norm.dat 40 V (32.6 Vbd)
    //../data/20.04.2023_05/C1wf00001.txt_norm.dat 44 V (32.6 Vbd)
    //../data/20.04.2023_05/C1wf00002.txt_norm.dat 48 V (32.6 Vbd)
    TGraph *gr_FBKampl_50um_meas40V = new TGraph();
    TGraph *gr_FBKampl_50um_meas44V = new TGraph();
    TGraph *gr_FBKampl_50um_meas48V = new TGraph();
    gr_FBKampl_50um_meas40V->SetNameTitle("gr_FBKampl_50um_meas40V","gr_FBKampl_50um_meas40V");
    gr_FBKampl_50um_meas44V->SetNameTitle("gr_FBKampl_50um_meas44V","gr_FBKampl_50um_meas44V");
    gr_FBKampl_50um_meas48V->SetNameTitle("gr_FBKampl_50um_meas48V","gr_FBKampl_50um_meas48V");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_50um_meas40V_C1wf00000.txt_norm.dat", gr_FBKampl_50um_meas40V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_50um_meas44V_C1wf00001.txt_norm.dat", gr_FBKampl_50um_meas44V, true, -300.0);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBKampl_50um_meas44V_C1wf00002.txt_norm.dat", gr_FBKampl_50um_meas48V, true, -300.0);
    //
    // meas_VS_model_25u.csv meas_VS_model_30u.csv meas_VS_model_35u.csv meas_VS_model_40u.csv
    //FBK_FBKampl_modle_25um.csv
    //FBK_FBKampl_modle_30um.csv
    //FBK_FBKampl_modle_35um.csv
    //FBK_FBKampl_modle_40um.csv
    //FBK_FBKampl_meas_25um.csv
    //FBK_FBKampl_meas_30um.csv
    //FBK_FBKampl_meas_35um.csv
    //FBK_FBKampl_meas_40um.csv
    TGraph *gr_FBK_FBKampl_modle_25um = new TGraph();
    TGraph *gr_FBK_FBKampl_modle_30um = new TGraph();
    TGraph *gr_FBK_FBKampl_modle_35um = new TGraph();
    TGraph *gr_FBK_FBKampl_modle_40um = new TGraph();
    gr_FBK_FBKampl_modle_25um->SetNameTitle("gr_FBK_FBKampl_modle_25um","gr_FBK_FBKampl_modle_25um");
    gr_FBK_FBKampl_modle_30um->SetNameTitle("gr_FBK_FBKampl_modle_30um","gr_FBK_FBKampl_modle_30um");
    gr_FBK_FBKampl_modle_35um->SetNameTitle("gr_FBK_FBKampl_modle_35um","gr_FBK_FBKampl_modle_35um");
    gr_FBK_FBKampl_modle_40um->SetNameTitle("gr_FBK_FBKampl_modle_40um","gr_FBK_FBKampl_modle_40um");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_modle_25um.csv", gr_FBK_FBKampl_modle_25um);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_modle_30um.csv", gr_FBK_FBKampl_modle_30um);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_modle_35um.csv", gr_FBK_FBKampl_modle_35um);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_modle_40um.csv", gr_FBK_FBKampl_modle_40um);
    //
    TGraph *gr_FBK_FBKampl_meas_25um = new TGraph();
    TGraph *gr_FBK_FBKampl_meas_30um = new TGraph();
    TGraph *gr_FBK_FBKampl_meas_35um = new TGraph();
    TGraph *gr_FBK_FBKampl_meas_40um = new TGraph();
    gr_FBK_FBKampl_meas_25um->SetNameTitle("gr_FBK_FBKampl_meas_25um","gr_FBK_FBKampl_meas_25um");
    gr_FBK_FBKampl_meas_30um->SetNameTitle("gr_FBK_FBKampl_meas_30um","gr_FBK_FBKampl_meas_30um");
    gr_FBK_FBKampl_meas_35um->SetNameTitle("gr_FBK_FBKampl_meas_35um","gr_FBK_FBKampl_meas_35um");
    gr_FBK_FBKampl_meas_40um->SetNameTitle("gr_FBK_FBKampl_meas_40um","gr_FBK_FBKampl_meas_40um");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_meas_25um.csv", gr_FBK_FBKampl_meas_25um);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_meas_30um.csv", gr_FBK_FBKampl_meas_30um);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_meas_35um.csv", gr_FBK_FBKampl_meas_35um);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_FBKampl_meas_40um.csv", gr_FBK_FBKampl_meas_40um);
    //
    //
    TGraph *gr_FBK_laser_34V_meas_25um = new TGraph();
    TGraph *gr_FBK_laser_34V_meas_30um = new TGraph();
    TGraph *gr_FBK_laser_34V_meas_35um = new TGraph();
    TGraph *gr_FBK_laser_34V_meas_50um = new TGraph();
    gr_FBK_laser_34V_meas_25um->SetNameTitle("gr_FBK_laser_34V_meas_25um","gr_FBK_laser_34V_meas_25um");
    gr_FBK_laser_34V_meas_30um->SetNameTitle("gr_FBK_laser_34V_meas_30um","gr_FBK_laser_34V_meas_30um");
    gr_FBK_laser_34V_meas_35um->SetNameTitle("gr_FBK_laser_34V_meas_35um","gr_FBK_laser_34V_meas_35um");
    gr_FBK_laser_34V_meas_50um->SetNameTitle("gr_FBK_laser_34V_meas_50um","gr_FBK_laser_34V_meas_50um");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_laser_34V_meas_25um_C2wf00003_r.txt", gr_FBK_laser_34V_meas_25um,true,0,true);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_laser_34V_meas_30um_C2wf00003_r.txt", gr_FBK_laser_34V_meas_30um,true,0,true);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_laser_34V_meas_35um_C2wf00003_r.txt", gr_FBK_laser_34V_meas_35um,true,0,true);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_laser_34V_meas_50um_C2wf00003_r.txt", gr_FBK_laser_34V_meas_50um,true,0,true);
    //
    //
    TGraph *gr_FBK_model_1pe_noampl_6vov = new TGraph();
    TGraph *gr_FBK_model_1pe_noampl_10vov = new TGraph();
    gr_FBK_model_1pe_noampl_6vov->SetNameTitle("gr_FBK_model_1pe_noampl_6vov","gr_FBK_model_1pe_noampl_6vov");
    gr_FBK_model_1pe_noampl_10vov->SetNameTitle("gr_FBK_model_1pe_noampl_10vov","gr_FBK_model_1pe_noampl_10vov");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_1pe_noampl_6vov.csv", gr_FBK_model_1pe_noampl_6vov,false,0,true);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_1pe_noampl_10vov.csv", gr_FBK_model_1pe_noampl_10vov,false,0,true);
    //
    //
    TGraph *gr_terzina_preampl_AC = new TGraph();
    gr_terzina_preampl_AC->SetNameTitle("gr_terzina_preampl_AC","gr_terzina_preampl_AC");
    conv_csv_to_gr("./reference_waveforms/csv_all/terzina_preampl_AC.csv", gr_terzina_preampl_AC);
    //
    //
    TGraph *gr_FBK_MT_30um_Vov7p8_Ntot_7303 = new TGraph();
    TGraph *gr_FBK_MT_30um_Vov7p8_Ntot_7098 = new TGraph();
    gr_FBK_MT_30um_Vov7p8_Ntot_7303->SetNameTitle("gr_FBK_MT_30um_Vov7p8_Ntot_7303","gr_FBK_MT_30um_Vov7p8_Ntot_7303");
    gr_FBK_MT_30um_Vov7p8_Ntot_7098->SetNameTitle("gr_FBK_MT_30um_Vov7p8_Ntot_7098","gr_FBK_MT_30um_Vov7p8_Ntot_7098");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_MT_30um_Vov7p8_Ntot_7303.csv", gr_FBK_MT_30um_Vov7p8_Ntot_7303);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_MT_30um_Vov7p8_Ntot_7098.csv", gr_FBK_MT_30um_Vov7p8_Ntot_7098);
    //
    //
    TGraph *gr_FBK_MT_ampl_30um_Vov6_Ntot_7098 = new TGraph();
    TGraph *gr_FBK_MT_ampl_30um_Vov10_Ntot_7098 = new TGraph();
    TGraph *gr_TERZINA_TERZINAampl_30um_N77098_1pe = new TGraph();
    TGraph *gr_TERZINA_TERZINAampl_30um_N77098_2pe = new TGraph();
    gr_FBK_MT_ampl_30um_Vov6_Ntot_7098->SetNameTitle("gr_FBK_MT_ampl_30um_Vov6_Ntot_7098","gr_FBK_MT_ampl_30um_Vov6_Ntot_7098");
    gr_FBK_MT_ampl_30um_Vov10_Ntot_7098->SetNameTitle("gr_FBK_MT_ampl_30um_Vov10_Ntot_7098","gr_FBK_MT_ampl_30um_Vov10_Ntot_7098");
    gr_TERZINA_TERZINAampl_30um_N77098_1pe->SetNameTitle("gr_TERZINA_TERZINAampl_30um_N77098_1pe","gr_TERZINA_TERZINAampl_30um_N77098_1pe");
    gr_TERZINA_TERZINAampl_30um_N77098_2pe->SetNameTitle("gr_TERZINA_TERZINAampl_30um_N77098_2pe","gr_TERZINA_TERZINAampl_30um_N77098_2pe");
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_MT_ampl_30um_Vov6_Ntot_7098.csv",gr_FBK_MT_ampl_30um_Vov6_Ntot_7098);
    conv_csv_to_gr("./reference_waveforms/csv_all/FBK_MT_ampl_30um_Vov10_Ntot_7098.csv",gr_FBK_MT_ampl_30um_Vov10_Ntot_7098);
    conv_csv_to_gr("./reference_waveforms/csv_all/TERZINA_TERZINAampl_30um_N77098_1pe.csv",gr_TERZINA_TERZINAampl_30um_N77098_1pe);
    conv_csv_to_gr("./reference_waveforms/csv_all/TERZINA_TERZINAampl_30um_N77098_2pe.csv",gr_TERZINA_TERZINAampl_30um_N77098_2pe);
    //
    TString outroot = "./reference_waveforms/conv_csv_to_gr.root";
    TFile* rootFile = new TFile(outroot.Data(), "RECREATE", " Histograms", 1);
    rootFile->cd();
    if (rootFile->IsZombie()){
      std::cout<<"  ERROR ---> file "<<outroot.Data()<<" is zombi"<<std::endl;
      assert(0);
    }
    gr_FBK_FBKampl_25um_Lpar5em09R5->Write();
    gr_FBK_FBKampl_25um_Lpar5em08R5->Write();
    gr_FBK_FBKampl_25um_Lpar5em08R10->Write();
    gr_FBK_FBKampl_25um_Lpar5em08R15->Write();
    gr_FBK_FBKampl_25um_Lpar2p5em08R50->Write();
    gr_FBK_FBKampl_25um_measurements->Write();
    //
    gr_FBKampl_40um_meas40V->Write();
    gr_FBKampl_40um_meas44V->Write();
    gr_FBKampl_40um_meas48V->Write();
    //
    gr_FBKampl_25um_meas40V->Write();
    gr_FBKampl_25um_meas44V->Write();
    gr_FBKampl_25um_meas48V->Write();
    //
    gr_FBKampl_30um_meas40V->Write();
    gr_FBKampl_30um_meas44V->Write();
    gr_FBKampl_30um_meas48V->Write();
    //
    gr_FBKampl_35um_meas40V->Write();
    gr_FBKampl_35um_meas44V->Write();
    gr_FBKampl_35um_meas48V->Write();
    //
    gr_FBKampl_50um_meas40V->Write();
    gr_FBKampl_50um_meas44V->Write();
    gr_FBKampl_50um_meas48V->Write();
    //
    gr_FBK_FBKampl_modle_25um->Write();
    gr_FBK_FBKampl_modle_30um->Write();
    gr_FBK_FBKampl_modle_35um->Write();
    gr_FBK_FBKampl_modle_40um->Write();
    //
    gr_FBK_FBKampl_meas_25um->Write();
    gr_FBK_FBKampl_meas_30um->Write();
    gr_FBK_FBKampl_meas_35um->Write();
    gr_FBK_FBKampl_meas_40um->Write();
    //
    gr_FBK_laser_34V_meas_25um->Write();
    gr_FBK_laser_34V_meas_30um->Write();
    gr_FBK_laser_34V_meas_35um->Write();
    gr_FBK_laser_34V_meas_50um->Write();
    //
    gr_FBK_model_1pe_noampl_6vov->Write();
    gr_FBK_model_1pe_noampl_10vov->Write();
    //
    gr_FBK_MT_30um_Vov7p8_Ntot_7303->Write();
    gr_FBK_MT_30um_Vov7p8_Ntot_7098->Write();
    //
    gr_FBK_MT_ampl_30um_Vov6_Ntot_7098->Write();
    gr_FBK_MT_ampl_30um_Vov10_Ntot_7098->Write();
    gr_TERZINA_TERZINAampl_30um_N77098_1pe->Write();
    gr_TERZINA_TERZINAampl_30um_N77098_2pe->Write();
    //
    gr_terzina_preampl_AC->Write();
    //
    rootFile->Close();
    std::cout<<"  Output TGrpah file ---> "<<outroot.Data()<<std::endl;
  }
  else{
    cout<<"  ERROR ---> in input arguments "<<endl
	<<"             [1] : 0"<<endl;
  }
  return 0;
}

void conv_csv_to_gr(TString txtFile, TGraph *gr, Bool_t if_conv_to_ns, Double_t t_shift_ns, Bool_t if_norm){
  string line;
  ifstream filein;
  filein.open(txtFile.Data());
  Double_t t, a;
  Double_t aMax=0.0, t_aMax;
  t_aMax = 0;
  if(filein.is_open()){
    while(filein>>t>>a){
      if(aMax<a){
	aMax = a;
	t_aMax = t;
      }
    }
    filein.close();
  }
  //
  filein.open(txtFile.Data());
  if(filein.is_open()){
    while(filein>>t>>a){
      if(if_conv_to_ns)
	t = t*1.0e+9;
      if(if_norm && aMax != 0.0){
	a = a/aMax;
	gr->SetPoint(gr->GetN(),t-t_aMax-t_shift_ns,a);
      }
      else{
	gr->SetPoint(gr->GetN(),t-t_shift_ns,a);
      }
      //cout << t << '\n';
    }
    filein.close();
  }
}
